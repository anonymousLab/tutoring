def ifStatements(n):
  print("------if Statements------")
  if n < 0:
    print("Negative num")
  elif n == 0:
    print("Zero")
  elif n == 1:
    print("Single")
  else:
    print("More")

def forStatements():
  print("------for Statements------")
  words = ['cat', 'window', 'defenestrate']
  for w in words:
    print(w, len(w))

  for i in range(5):
    print(i)

  a = ['Mary', 'had', 'a', 'little', 'lamb']
  for i in range(len(a)):
    print(i, a[i])

  '''
  # same code
  for i, e in enumerate(a):
    print(i, e)
  '''

  for n in range(2, 10):
    for x in range(2, n):
      if n % x == 0:
        print(n, 'equals', x, '*', n // x)
        break
      else:
        print(n, 'is a prime number')

  for n in range(2, 10):
    if n % 2 == 0:
      print("Found an even number", n)
      continue
    print("Found a number", n)

def fib(n):
  a, b = 0, 1
  while a < n:
    print(a, end=' ')
    a, b = b, a + b
  print()

i = 5
def f(arg=i):
  print(arg)

def parrot(voltage, state='a stiff', action='voom', type='Norwegian Blue'):
  print("-- This parrot wouldn't", action, end=' ')
  print("if you put", voltage, "volts through it.")
  print("-- Lovely plumage, the", type)
  print("-- It's", state, "!")

def cheeseshop(kind, *arguments, **keywords):
  print("-- Do you have any", kind, "?")
  print("-- I'm sorry, we're all out of", kind)
  for arg in arguments:
    print(arg)
  print("-" * 40)
  for kw in keywords:
    print(kw, ":", keywords[kw])

def functions():
  fib(200)
  f()
  parrot(1000)                                          # 1 positional argument
  parrot(voltage=1000)                                  # 1 keyword argument
  parrot(voltage=1000000, action='VOOOOOM')             # 2 keyword arguments
  parrot(action='VOOOOOM', voltage=1000000)             # 2 keyword arguments
  parrot('a million', 'bereft of life', 'jump')         # 3 positional arguments
  parrot('a thousand', state='pushing up the daisies')  # 1 positional, 1 keyword
  cheeseshop("Limburger", "It's very runny, sir.",
             "It's really very, VERY runny, sir.",
             shopkeeper="Michael Palin",
             client="John Cleese",
             sketch="Cheese Shop Sketch")

def lambdaExpressions():
  pairs = [(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four')]
  pairs.sort(key=lambda pair: pair[1])
  print(pairs)

if __name__ == "__main__":
  ifStatements(0)
  forStatements()
  functions()
  lambdaExpressions()
