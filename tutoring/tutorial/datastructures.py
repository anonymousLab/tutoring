from collections import deque

def lists():
  fruits = ['orange', 'apple', 'pear', 'banana', 'kiwi', 'apple', 'banana']
  print(fruits.count('apple'))
  print(fruits.index('banana'))
  print(fruits.index('banana', 4))
  print(fruits.reverse())
  fruits.append('grape')
  print(fruits)
  fruits.sort()
  print(fruits)
  print(fruits.pop())

  stack = [3, 4, 5]
  stack.append(6)
  print(stack)
  stack.pop()
  print(stack)

  queue = deque(["Eric", "John", "Michael"])
  queue.append("Terry")
  queue.popleft()
  print(queue)

  squares = []
  for x in range(10):
    squares.append(x ** 2)
  print(squares)
  squares2 = list(map(lambda x: x ** 2, range(10)))
  squares3 = [x ** 2 for x in range(10)]
  print(squares, squares2, squares3)

  vec = [-4, -2, 0, 2, 4]
  l = [x*2 for x in vec]
  l1 = [x for x in vec if x >= 0]
  l2 = [abs(x) for x in vec]
  l3 = [(x, x**2) for x in range(6)]
  print(l, l1, l2, l3)

  matrix = [[1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12],]

  print(matrix)
  m1 = [[row[i] for row in matrix] for i in range(4)]
  m2 = []
  for i in range(4):
    m2.append([row[i] for row in matrix])
  m3 = []
  for i in range(4):
    transposed_row = []
    for r in matrix:
      transposed_row.append(r[i])
    m3.append(transposed_row)

  print(m1, m2, m3)

  answer = list(zip(*matrix))
  print(answer)

  a = [-1, 1, 66.25, 333, 333, 1234.5]
  print(a)
  del a[0]
  print(a)

  del a[:]
  print(a)

def tuples():
  t = 12345, 54321, 'hello!'
  print(t)
  u = t, (1, 2, 3, 4, 5)
  print(u)
  # t[0] = 88888
  v = ([1, 2, 3], [3, 2, 1])
  print(v)

def sets():
  basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
  print(basket)
  a = set('abracadabra')
  b = set('alacazam')
  print(a, b)
  print(a - b)
  print(a | b)
  print(a & b)
  print(a ^ b)

def dictionaries():
  tel = {'jack': 4098, 'sape': 4139}
  tel['guido'] = 4127
  print(tel)
  del tel['sape']
  print(tel)
  print(list(tel))

def loops():
  knights = {'gallahad': 'the pure', 'robin': 'the brave'}
  for k, v in knights.items():
    print(k, v)

  for i, v in enumerate(['tic', 'tac', 'toe']):
    print(i, v)

  questions = ['name', 'quest', 'favorite color']
  answers = ['lancelot', 'the holy grail', 'blue']
  for q, a in zip(questions, answers):
    print('What is your {0}?  It is {1}.'.format(q, a))

  for i in reversed(range(1, 10, 2)):
    print(i)

  basket = ['apple', 'orange', 'apple', 'pear', 'orange', 'banana']
  for f in sorted(set(basket)):
    print(f)

if __name__ == "__main__":
  lists()
  tuples()
  sets()
  dictionaries()
  loops()
