def numbers():
  print("------numbers------")
  print(2 + 2)
  print(50 - 5 * 6)
  print((50 - 5 * 6) / 4)
  print(8 / 5)
  print(17 / 3)
  print(17 // 3)
  print(17 % 3)
  print(5 * 3 + 2)
  print(2 ** 7) # 2 to the power of 7

  width = 20
  height = 5 * 9
  print(width * height)

  tax = 12.5 / 100
  price = 100.50
  price = tax * price
  print(round(price, 2))

def strings():
  print("------strings------")
  print("spam eggs")
  print("doesn\'t")
  print('"Yes," they said.')
  print("\"Yes,\" they said.")
  print('"Isn\'t," they said.')
  s = 'First line.\nSecond line.'
  print(s)
  print('C:\some\name')
  print(r'C:\some\name')
  print(3 * 'un' + 'ium')
  prefix = 'Py'
  print(prefix + 'thon')
  word = 'Python'
  print(word[0])
  print(word[5])
  print(word[-1])
  print(word[0:2])
  print(word[4:])
  # strings are immutable
  print(len(word))

def lists():
  print("------lists------")
  squares = [1, 4, 9, 16, 25]
  print(squares)
  print(squares[0])
  print(squares[-3:])
  print(squares[:])
  squares = squares + [36, 49, 64, 81, 100]
  print(squares)
  cubes = [1, 8, 27, 65, 125]
  cubes.append(216)
  cubes.append(7 ** 3)
  print(cubes)
  letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
  print(letters[2:5])
  letters[2:5] = []
  print(letters)
  letters[:] = []
  letters = ['a', 'b', 'c', 'd']
  print(len(letters))
  a = ['a', 'b', 'c']
  n = [1, 2, 3]
  x = [a, n]
  print(x)
  print(x[0])
  print(x[0][1])

if __name__ == "__main__":
  numbers()
  strings()
  lists()
