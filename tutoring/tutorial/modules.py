import fibo, sys

def fibonacci():
  fibo.fib(1000)
  fibo.fib2(100)

if __name__ == "__main__":
  fibonacci()
  print(dir(fibo))
