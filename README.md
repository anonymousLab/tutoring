# Tutorial session
This tutorial session is for whom wants to learn quick basics.

## 1. Introduction(19.06.16)
Let's get started with **a black window** and have fun coding.

### Do Coding without Hesitation
1. Open terminal
2. Make your own work directory
3. Type "touch hello.py"
4. Open editor; Visual Studio Code
5. Copy and Paste below:
```
def main():
  print("hello")

if __name__ == "__main__":
  main()
```
6. Save the file
7. Do "python hello.py"

### Easy to Use Command Line
**.** current dir  
**..** previous dir  
**cd** dirName-> move to directory  
**mv** file toDir -> move file to where I want  
**cp** file toDir -> copy file to where I want  
**touch** fileName -> make empty file  
**rm** fileName -> remove file  
**mkdir** dirName -> make directory

### Python Project Structure
This is an usual project-looks like structure of Python project.  
(reference; https://dev.to/codemouse92/dead-simple-python-project-structure-and-imports-38c6)
```
omission-git
├── LICENSE.md
├── omission
│   ├── app.py
│   ├── common
│   │   ├── classproperty.py
│   │   ├── constants.py
│   │   ├── game_enums.py
│   │   └── __init__.py
│   ├── data
│   │   ├── data_loader.py
│   │   ├── game_round_settings.py
│   │   ├── __init__.py
│   │   ├── scoreboard.py
│   │   └── settings.py
│   ├── game
│   │   ├── content_loader.py
│   │   ├── game_item.py
│   │   ├── game_round.py
│   │   ├── __init__.py
│   │   └── timer.py
│   ├── __init__.py
│   ├── __main__.py
│   ├── resources
│   └── tests
│       ├── __init__.py
│       ├── test_game_item.py
│       ├── test_game_round_settings.py
│       ├── test_scoreboard.py
│       ├── test_settings.py
│       ├── test_test.py
│       └── test_timer.py
├── README.md
└── .gitignore
```
## 2. Introduction to Git and Project(19.06.19)
Let's get started with **GIT** and **PROJECT MANAGEMENT**
Have fun coding.

### Before Getting Started
Copy below
```
git@gitlab.com:anonymousLab/tutoring.git
```

### Initialize My Project
This is a tutorial project looks-like for now.
```
tutoring/
├── LICENSE
├── README.md
└── tutoring
    ├── __init__.py
    ├── __main__.py
    ├── hello.py
    └── tutorial
        ├── __init__.py
        ├── controlflow.py
        ├── datastructures.py
        ├── fibo.py
        ├── modules.py
        └── tutorial.py
```

### Demystify **GIT** on Command Line
**git clone** projectURL -> clone a project I want  
**git status** -> check current git status  
**git log** -> check logs of all commits  
**git add** filename  
-> when new file added on git  
-> (when multiple files are updated) pick files that I want to commit  
**git commit** -m "message" -> commit with message  
**git commit** -a -> commit all staged things  
**git push** -> local commit to a remote server  
**git pull** -> Update local git repo to latest commit on a remote server  
**git stash**  
**git stash pop**  
**git blame**  
**git reset**  
**git merge**  
**git checkout**  
**git branch**  
regarding on branch
